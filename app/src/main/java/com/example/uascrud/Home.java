package com.example.uascrud;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Home extends AppCompatActivity {

    DatabaseHelper myDb;
    EditText userName, password, gender, dateOfBirth, editTextId;
    Button btnAddData;
    Button btnUpdate;
    Button btnDelete;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        myDb = new DatabaseHelper(this);

        editTextId = (EditText) findViewById(R.id.editTextTextPersonID);
        userName = (EditText) findViewById(R.id.editTextTextPersonUserName);
        password = (EditText) findViewById(R.id.editTextTextPersonPassword);
        gender = (EditText) findViewById(R.id.editTextTextPersonGender);
        dateOfBirth = (EditText) findViewById(R.id.editTextTextPersonDateOfBirth);
        btnAddData = (Button) findViewById(R.id.btnAddData);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);


        userName.setText(myDb.getUsername());

        Cursor c = myDb.getAllData(myDb.getUsername());
        if (c.moveToNext()) {
            editTextId.setText(c.getString(0));
            userName.setText(c.getString(1));
            password.setText(c.getString(2));
            gender.setText(c.getString(3));
            dateOfBirth.setText(c.getString(4));
        }

        DeleteData();
        UpdateData();

    }

    /**
     *
     */
    public void clear() {
        editTextId.setText("");
        userName.setText("");
        password.setText("");
        gender.setText("");
        dateOfBirth.setText("");
    }

    /**
     *
     */
    public void DeleteData() {
        btnDelete.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Integer deletedRows = myDb.deleteData(editTextId.getText().toString());
                        if (deletedRows > 0)
                            Toast.makeText(Home.this, "Data Deleted", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(Home.this, "Data not Deleted", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    /**
     *
     */
    public void UpdateData() {
        btnUpdate.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isUpdate = myDb.updateData(editTextId.getText().toString(),
                                userName.getText().toString(),
                                password.getText().toString(), gender.getText().toString(), dateOfBirth.getText().toString());
                        if (isUpdate == true)
                            Toast.makeText(Home.this, "Data Updated", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(Home.this, "Data not Updated", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }
}
