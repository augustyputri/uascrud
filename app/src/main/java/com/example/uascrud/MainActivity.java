package com.example.uascrud;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //Creating DatabaseHelper Object
    DatabaseHelper myDb;
    EditText userName, password, gender, dateOfBirth, editTextId;
    Button btnAddData;
    Button btnviewAll;
    Button btnDelete;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myDb = new DatabaseHelper(this);

        editTextId = (EditText) findViewById(R.id.editTextTextPersonID);
        userName = (EditText) findViewById(R.id.editTextTextPersonUserName);
        password = (EditText) findViewById(R.id.editTextTextPersonPassword);
        gender = (EditText) findViewById(R.id.editTextTextPersonGender);
        dateOfBirth = (EditText) findViewById(R.id.editTextTextPersonDateOfBirth);
        btnAddData = (Button) findViewById(R.id.btnAddData);
        btnLogin = (Button) findViewById(R.id.btnLogin);


        //Invoking AddData
        AddData();
        //Invoking BtnLogin
        BtnLogin();
    }

    private void BtnLogin() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Opening new activity on button click.
                openActivity2();
            }
        });
    }

    public void openActivity2() {
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }


    public void AddData() {
        btnAddData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isInserted = myDb.insertData(userName.getText().toString(),
                        password.getText().toString(),
                        gender.getText().toString(),
                        dateOfBirth.getText().toString());
                if (isInserted == true)
                    Toast.makeText(MainActivity.this, "Data Inserted", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(MainActivity.this, "Data not Inserted", Toast.LENGTH_LONG).show();
            }
        });
    }

}