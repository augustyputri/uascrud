package com.example.uascrud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    DatabaseHelper myDb;
    EditText userName, password, gender,dateOfBirth, editTextId;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        myDb = new DatabaseHelper(this);

        userName = (EditText)findViewById(R.id.editTextTextPersonUserName);
        password = (EditText)findViewById(R.id.editTextTextPersonPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        login();

    }


    public void goToHome() {
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }


    private void login() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isInserted = myDb.checkUser(userName.getText().toString(),
                        password.getText().toString()
                );
                if(isInserted == true) {
                    myDb.setUsername(userName.getText().toString());
                    Toast.makeText(Login.this, "Logged In As " + userName.getText().toString(), Toast.LENGTH_LONG).show();
                    goToHome();
                }
                else
                    Toast.makeText(Login.this,"Not Logged In",Toast.LENGTH_LONG).show();
            }
        });
    }
}


